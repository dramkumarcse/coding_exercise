require 'test_helper'

class ModelTypesControllerTest < ActionController::TestCase
  setup do
    @model_type = model_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:model_types)
  end

  test "should create model_type" do
    assert_difference('ModelType.count') do
      post :create, model_type: { car_model_id: @model_type.car_model_id, model_type_code: @model_type.model_type_code, name: @model_type.name, price: @model_type.price, slug: @model_type.slug }
    end

    assert_response 201
  end

  test "should show model_type" do
    get :show, id: @model_type
    assert_response :success
  end

  test "should update model_type" do
    put :update, id: @model_type, model_type: { car_model_id: @model_type.car_model_id, model_type_code: @model_type.model_type_code, name: @model_type.name, price: @model_type.price, slug: @model_type.slug }
    assert_response 204
  end

  test "should destroy model_type" do
    assert_difference('ModelType.count', -1) do
      delete :destroy, id: @model_type
    end

    assert_response 204
  end
end
