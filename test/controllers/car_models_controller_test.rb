require 'test_helper'

class CarModelsControllerTest < ActionController::TestCase
  setup do
    @car_model = car_models(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:car_models)
  end

  test "should create car_model" do
    assert_difference('CarModel.count') do
      post :create, car_model: { name: @car_model.name, organization_id: @car_model.organization_id, slug: @car_model.slug }
    end

    assert_response 201
  end

  test "should show car_model" do
    get :show, id: @car_model
    assert_response :success
  end

  test "should update car_model" do
    put :update, id: @car_model, car_model: { name: @car_model.name, organization_id: @car_model.organization_id, slug: @car_model.slug }
    assert_response 204
  end

  test "should destroy car_model" do
    assert_difference('CarModel.count', -1) do
      delete :destroy, id: @car_model
    end

    assert_response 204
  end
end
