class ApplicationController < ActionController::API
  include ActionController::MimeResponds

  rescue_from ActiveRecord::RecordNotFound do |exception|
    record_not_found(exception)
  end

 	def record_not_found(e)
    render json: { resp: false, info: nil, msg: e.message }, status: :not_found
  end

  def missing_params(e)
    render json: { resp: e.status, info: e.info, msg: e.message }, status: 412
  end

  def show_response(status, data, **args)
    opts = { :paginate? => true }
    msg = args.fetch(:message, "")
    args.delete(:message)

    opts = opts.merge(args)
    page_info = {}
    if opts[:paginate?]
      pd = paginate(data, per_page: 10)
      page_info = { current_page: pd.current_page, prev_page: pd.prev_page, next_page: pd.next_page, total_pages: pd.total_pages, total: pd.total_count }
    else
      pd = data
    end

    resp = opts.merge(json: pd, status: status, meta: { status: status, message: msg, page_info: page_info })
    render resp
  end

  def default_serializer_options
    { root: :data }
  end

end
