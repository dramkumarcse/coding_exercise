module Api
  module V1
    class CarModelsController < API::V1::BaseController
      before_action :set_car_model, only: [:show, :update, :destroy]


      def index
        car_models = CarModel.all
        show_response(200, car_models, message: "Car model list", context: {tag: car_models })
      end


      def show
        show_response(200, [car_model], message: "car model", context: {tag: car_model })
      end


      def create
        car_model = CarModel.new(car_model_params)

        if car_model.save
          show_response(200, [car_model], message: "Car created", context: {tag: car_model })
        else
          show_response(412, car_model.errors, message: "", context: {tag: car_model})
        end
      end


      def update
        car_model = CarModel.find(params[:id])

        if car_model.update(car_model_params)
          show_response(200, [car_model], message: "Car created", context: {tag: car_model })
        else
          show_response(412, car_model.errors, message: "", context: {tag: car_model })
        end
      end

      def destroy
        car_model.destroy

        show_response(200, [], message: "Car deleted", context: {})
      end

      private

      def set_car_model
        car_model = CarModel.find(params[:id])
      end

      def car_model_params
        params.require(:car_model).permit(:name, :slug, :organization_id)
      end
    end
  end
end
