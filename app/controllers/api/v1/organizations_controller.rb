module Api
  module V1
    class OrganizationsController < API::V1::BaseController
      before_action :set_organization, only: [:show, :update, :destroy]

      # GET /organizations
      # GET /organizations.json
      def index
        organizations = Organization.all

        show_response(200, organizations, message: "organizations list", context: {tag: organizations })
      end

      # GET /organizations/1
      # GET /organizations/1.json
      def show
        show_response(200, [organization], message: "organizations list", context: {tag: organization })
      end

      # POST /organizations
      # POST /organizations.json
      def create
        organization = Organization.new(organization_params)

        if organization.save
          show_response(200, [organization], message: "organizations list", context: {tag: organization })
        else
          show_response(412, organization.errors, message: "", context: {tag: organization })
        end
      end

      # PATCH/PUT /organizations/1
      # PATCH/PUT /organizations/1.json
      def update
        organization = Organization.find(params[:id])

        if organization.update(organization_params)
          show_response(200, [organization], message: "organizations list", context: {tag: organization })
        else
          show_response(412, organization.errors, message: "", context: {tag: organization })
        end
      end

      # DELETE /organizations/1
      # DELETE /organizations/1.json
      def destroy
        organization.destroy

        show_response(200, [], message: "Organizations deleted", context: {})
      end

      private

      def set_organization
        organization = Organization.find(params[:id])
      end

      def organization_params
        params.require(:organization).permit(:name, :public_name, :org_type, :pricing_policy)
      end
    end
  end
end
