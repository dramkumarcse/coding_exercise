module Api
  module V1
    class ModelTypesController < API::V1::BaseController
      before_action :set_model_type, only: [:show, :update, :destroy]

      # GET /model_types
      # GET /model_types.json
      def index
        model_types = ModelType.all

        show_response(200, model_types, message: "model types list", context: {tag: model_types })
      end

      # GET /model_types/1
      # GET /model_types/1.json
      def show
        show_response(200, [model_type], message: "model types list", context: {tag: model_type })
      end

      # POST /model_types
      # POST /model_types.json
      def create
        model_type = ModelType.new(model_type_params)

        if model_type.save
          show_response(200, [model_type], message: "model types list", context: {tag: model_type })
        else
          show_response(412, model_type.errors, message: "", context: {tag: model_type })
        end
      end

      # PATCH/PUT /model_types/1
      # PATCH/PUT /model_types/1.json
      def update
        model_type = ModelType.find(params[:id])

        if model_type.update(model_type_params)
          show_response(200, [model_type], message: "model types list", context: {tag: model_type })
        else
          show_response(412, model_type.errors, message: "", context: {tag: model_type })
        end
      end

      # DELETE /model_types/1
      # DELETE /model_types/1.json
      def destroy
        model_type.destroy

        show_response(200, [], message: "Model type deleted", context: {})
      end

      private

      def set_model_type
        model_type = ModelType.find(params[:id])
      end

      def model_type_params
        params.require(:model_type).permit(:name, :slug, :model_type_code, :price, :car_model_id)
      end
    end
  end
end
