require 'car_price'
class ModelType < ActiveRecord::Base
  include CarPrice
  belongs_to :car_model

  def total_price(price = nil)
    price = price || self.price
    policy = self.car_model.organization.pricing_policy
    CarPrice.compute_price(policy, price)
  end

end
