class OrganizationSerializer < ActiveModel::Serializer
  attributes :id, :name, :public_name, :org_type, :pricing_policy
end
