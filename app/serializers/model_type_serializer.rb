class ModelTypeSerializer < ActiveModel::Serializer
  attributes :id, :name, :slug, :model_type_code, :price, :car_model_id , :total_price
end
