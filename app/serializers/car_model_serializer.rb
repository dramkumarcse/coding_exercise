class CarModelSerializer < ActiveModel::Serializer
  attributes :id, :name, :slug, :organization_id
end
