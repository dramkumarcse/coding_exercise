class CreateModelTypes < ActiveRecord::Migration
  def change
    create_table :model_types do |t|
      t.string :name
      t.string :slug, unique: true
      t.string :model_type_code
      t.string :price
      t.integer :car_model_id

      t.timestamps null: false
    end
  end
end
