
module CarPrice
  require 'nokogiri'
  require 'rss'
  require 'open-uri'

  def self.compute_price(policy, price)
    total_price = 0
    price = price.to_i
    case policy

    when "Flexible"
      url = 'http://reuters.com'
      text = page_content(url)
      a_count = text.count('a')
      margin = a_count/100
      total_price = price * margin
    when "Fixed"
      url = 'https://developer.github.com/v3/#http-redirects'
      text = page_content(url)
      words = text.scan(/\w+/)
      count = words.count("status") #case sensitive
      margin = count
      total_price = price + margin
    when "Prestige"
      url = 'http://reuters.com'
      rss = RSS::Parser.parse('http://www.yourlocalguardian.co.uk/sport/rugby/rss/', false)
      pubDate_count = 0
      rss.items.each do |item|
        pubDate_count += 1 if item.respond_to? "pubDate"
      end
      margin = pubDate_count
      total_price = price + margin
    end
    total_price
  end

  def self.page_content(url)
    html = open(url,:ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE)
    doc = Nokogiri.HTML(html)
    doc.css('script').remove
    doc.css('style').remove
    text  = doc.at('body').inner_text
  end
end
